#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>
#include <numeric>
#include <boost/lambda/lambda.hpp>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::salary, _1) > 3000.0);

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::age, _1) < 30);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";

// bez operatora >
//    sort(employees.begin(), employees.end(),
//         boost::bind(greater<string>(),
//                        boost::bind(&Person::name, _1),
//                        boost::bind(&Person::name, _2)));

    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::gender, _1) == Female);

    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
    double avg = accumulate(employees.begin(), employees.end(), 0,
                            boost::bind(plus<double>(), _1, boost::bind(&Person::salary, _2))) / employees.size();

    cout << count_if(employees.begin(), employees.end(), boost::bind(&Person::salary, _1) > avg) << endl;

    vector<Person*> ptr_emps;
}
