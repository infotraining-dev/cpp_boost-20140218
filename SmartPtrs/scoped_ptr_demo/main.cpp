#include <iostream>
#include <stdexcept>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

using namespace std;

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};

void may_throw()
{
    throw std::runtime_error("ERROR");
}

void foo()
{
    boost::scoped_ptr<X> ptrx(new X(10));

    boost::scoped_ptr<X> ptrx2(new X(20));

    cout << "value of ptrx: " << ptrx->value() << endl;
    cout << "value of ptrx2: " << ptrx2->value() << endl;

    ptrx.swap(ptrx2);

    cout << "value of ptrx: " << ptrx->value() << endl;
    cout << "value of ptrx2: " << ptrx2->value() << endl;

    ptrx.reset();  // jawne wywolanie delete

    cout << "End of foo" << endl;
}

int main()
{
    foo();

    boost::scoped_array<X> arrX(new X[10]);

    arrX[0].set_value(7);

    return 0;
}

