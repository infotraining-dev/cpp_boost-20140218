#include <iostream>
#include <set>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace std;


class Observer
{
public:
  virtual void update(const std::string& event_args) = 0;
  virtual ~Observer() {}
};

class Subject
{
  int state_;
  std::set<boost::weak_ptr<Observer> > observers_;

public:
  Subject() : state_(0)
  {}

  void register_observer(boost::weak_ptr<Observer> observer)
  {
    observers_.insert(observer);
  }

  void unregister_observer(boost::weak_ptr<Observer> observer)
  {
      cout << "unregister_observer" << endl;
      observers_.erase(observer);
  }

  void set_state(int new_state)
  {
      if (state_ != new_state)
      {
          state_ = new_state;
          notify("Changed state on: " + boost::lexical_cast<std::string>(state_));
      }
  }

protected:
  void notify(const std::string& event_args)
  {
      //std::for_each(observers_.begin(), observers_.end(), boost::bind(&Observer::update, _1, event_args));

      std::set<boost::weak_ptr<Observer> >::iterator it = observers_.begin();
      while(it != observers_.end())
      {
          boost::shared_ptr<Observer> observer = it->lock();
          if (observer)
          {
              observer->update(event_args);
              ++it;
          }
          else
          {
              observers_.erase(it++);
          }
      }
  }
};

class ConcreteObserver1 : public Observer, public boost::enable_shared_from_this<ConcreteObserver1>
{
public:
  virtual void update(const std::string& event)
  {
    std::cout << "ConcreteObserver1: " << event << std::endl;
  }

   void register_itself(Subject& subject)
   {
       subject.register_observer(shared_from_this());
   }
};

class ConcreteObserver2 : public Observer
{
public:
  virtual void update(const std::string& event)
  {
    std::cout << "ConcreteObserver2: " << event << std::endl;
  }
};

class Base
{
public:
    virtual void do_sth()
    {
        cout << "Base::do_sth()" << endl;
    }

    virtual ~Base() {}
};

class Derived : public Base
{
public:
    virtual void do_sth()
    {
        cout << "Derived::do_sth()" << endl;
    }

    void derived_only()
    {
        cout << "Derived::derived_only()" << endl;
    }
};

boost::shared_ptr<Base> factory(int id)
{
    if (id == 1)
        return boost::make_shared<Base>();
    else if (id == 2)
        return boost::make_shared<Derived>();

    throw std::runtime_error("Bad ID");
}

int main(int argc, char const *argv[])
{
  using namespace std;

  Subject s;

  boost::shared_ptr<ConcreteObserver1> o1 = boost::make_shared<ConcreteObserver1>();
  //s.register_observer(o1);
  o1->register_itself(s);

  {
      boost::shared_ptr<ConcreteObserver2> o2 = boost::make_shared<ConcreteObserver2>();
      //boost::shared_ptr<void> scoped_unregister(0, boost::bind(&Subject::unregister_observer, &s, o2));

      s.register_observer(o2);

      s.set_state(1);

      //s.unregister_observer(o2);

      cout << "End of scope." << endl;
  }

  s.set_state(2);

  boost::shared_ptr<Base> ptrb = factory(1);

  ptrb->do_sth();

  boost::shared_ptr<Derived> ptrd = boost::dynamic_pointer_cast<Derived>(ptrb);

  if (ptrd)
      ptrd->derived_only();
}
