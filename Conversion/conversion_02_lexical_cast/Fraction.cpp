// do��cza plik nag��wkowy zawieraj�cy deklaracj� klasy
#include "Fraction.hpp"

/* domy�lny konstruktor i konstruktor o jednym i dw�ch parametrach
 * - domy�lna warto�� parametru n: 0
 * - domy�lna warto�� parametru d: 1
 */
Fraction::Fraction(int n, int d)
{
    /* inicjuje licznik i mianownik warto�ciami przekazanymi przez parametry
     * - warto�� 0 nie jest dozwolona dla mianownika,
     * - przenosi znak minus (je�li istnieje) z mianownika do licznika 
     */
    if (d == 0) {
        // nowo��: wyrzuca wyj�tek, gdy mianownik r�wny 0
        throw DenomIsZero();
    }
    if (d < 0) {
        numer = -n;
        denom = -d;
    }
    else {
        numer = n;
        denom = d;
    }
}

/* operator *=
 */
const Fraction& Fraction::operator *= (const Fraction& f)
{
    // `x *= y'  =>  `x = x * y'
    *this = *this * f;

    // zwraca pierwszy argument
    return *this;
}

/* operator <
 * - globalna funkcja zaprzyja�niona
 */
bool operator < (const Fraction& a, const Fraction& b)
{
    // poniewa� mianownik nie mo�e by� ujemny, to wystarcza:
    return a.numer * b.denom < b.numer * a.denom;
}

/* printOn
 * - zapis u�amka do strumienia strm
 */
void Fraction::printOn(std::ostream& strm) const
{
    strm << numer << '/' << denom;
}

/* scanFrom
 * - odczyt u�amka ze strumienia strm
 */
void Fraction::scanFrom(std::istream& strm)
{
    int n, d;

    // wczytuje licznik
    strm >> n;

    // wczytuje opcjonalny znak '/' i mianownik
    if (strm.peek() == '/') {
        strm.get();
        strm >> d;
    }
    else {
        d = 1;
    }

    // b��d odczytu?
    if (! strm) {
        return;
    }

    // mianownik r�wny 0?
    if (d == 0) {
        // nowo��: wyrzuca wyj�tek, gdy mianownik r�wny 0
        throw DenomIsZero();
    }

    /* przypisuje wczytane warto�ci sk�adowym
     * - przenosi znak minus z mianownika do licznika
     */
    if (d < 0) {
        numer = -n;
        denom = -d;
    }
    else {
        numer = n;
        denom = d;
    }
}

// konwersja do typu double
double Fraction::toDouble() const
{
    // zwraca wynik dzielenia licznika przez mianownik
    return double(numer)/double(denom);
}

