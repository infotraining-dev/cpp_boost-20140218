#define NDEBUG
#include <iostream>
#include <string>
#include <stdexcept>
#include <exception>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>
#include <bitset>
#include "Vehicle.hpp"

void travel_on_land(Vehicle* v, int distance)
{
//    Car& c = dynamic_cast<Car&>(*v);
//    c.drive(distance);

    Car* c = boost::polymorphic_cast<Car*>(v);
    c->drive(distance);
}

void travel_on_sea(Vehicle* v, int distance)
{
	Boat* b = boost::polymorphic_cast<Boat*>(v);
	b->travel(distance);
}

Vehicle* create_vehicle(const std::string& type_id, const std::string& vehicle_id)
{
	if (type_id == "Car")
		return new Car(vehicle_id);
	else if (type_id == "Boat")
		return new Boat(vehicle_id);
	throw std::invalid_argument("Invalid type_id");
}

int main()
{
    using namespace std;

#   ifdef NDEBUG
		std::cout << "NDEBUG is defined\n";
#   else
		std::cout << "NDEBUG is not defined\n";
#   endif


	// 1 - polymorphic_cast

	Vehicle* vhc = create_vehicle("Car", "KR123444");

	// ok
	travel_on_land(vhc, 10);
	vhc->info();

	// zle
	try
	{
		travel_on_sea(vhc, 20);
	}
	catch(const std::bad_cast& e)
	{
		std::cout << "Niepoprawne rzutowanie: " << e.what() << std::endl;
	}

	delete vhc;

	// 2 - polymorphic_downcast
	vhc = create_vehicle("Car", "PL_WW22344");

	Boat* b = boost::polymorphic_downcast<Boat*>(vhc);

	b->travel(100);
	b->info();

	delete b;

    // rzutowanie numeryczne
    int s = -77;

    try
    {
        unsigned char uc = boost::numeric_cast<unsigned char>(s);

        cout << static_cast<int>(uc) << endl;
    }
    catch(const boost::bad_numeric_cast& e)
    {
        cout << e.what() << endl;
    }

    string str = "3.14";

    double pi = boost::lexical_cast<double>(str);

    cout << "pi = " << pi << endl;

    string message = "pi " + boost::lexical_cast<string>(pi);
}
