#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>

using namespace std;

using namespace boost;
using namespace boost::signals2;

void callback1()
{
    cout << "callback" << endl;
}

class Callback1
{
    string id_;
public:
    Callback1(const string& id) : id_(id) {}

    void operator()()
    {
        cout << "Callback " << id_ << endl;
    }
};

void printer(const string& msg)
{
    cout << msg << endl;
}

void on_mouse_click(unsigned int x, unsigned int y)
{
    cout << "Update mouse position " << x << " " << y << endl;
}

#include <algorithm>

int slotA(int x) { return 42+x; }
int slotB(int x) { return 00100100+x; }
int slotC(int x) { return 0xabc+x; }

template <typename T>
class PrintCombiner
{
public:
  typedef T result_type;

  template<typename InputIterator>
  T operator()(InputIterator first, InputIterator last) const
  {
    for_each(first, last, [](T result){ std::cout << result << " "; } );
    std::cout << std::endl;
    return *first;
  }
};

int main()
{
    signal<void ()> sig1;

    Callback1 call1("1");
    Callback1 call2("2");
    Callback1 call3("3");

    sig1.connect(2, &callback1);
    connection conn2callback2 = sig1.connect(2, &callback1);
    sig1.connect(1, boost::ref(call1));
    sig1.connect(1, boost::ref(call2));
    sig1.connect(1, boost::ref(call3), at_front);
    sig1.connect(0 ,boost::bind(&printer, "Print when signalled"));

    sig1();

    cout << "\nDisconnect" << endl;

    //sig1.disconnect(&callback1);
    conn2callback2.disconnect();

    sig1();

    cout << "\n";

    signal<void (int, int)> mouse_clicked;

    mouse_clicked.connect(&on_mouse_click);

    mouse_clicked(10, 20);

    cout << "\n\n--------------------\n\n";
    signal< int (int), PrintCombiner<int> > sig;
    sig.connect( &slotA );
    sig.connect( &slotB );
    sig.connect( &slotC );

    std::cout << "[sig] = " << sig(1348) << std::endl;

}

