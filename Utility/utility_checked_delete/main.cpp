#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/ptr_container/ptr_vector.hpp>

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
    using namespace std;

	real_test();

    boost::ptr_vector<ToBeDeleted> vec;

    vec.push_back(new ToBeDeleted());
    vec.push_back(new ToBeDeleted());
    //vec.push_back(0);

   for(boost::ptr_vector<ToBeDeleted>::iterator it = vec.begin(); it != vec.end(); ++it)
       it->print();

//    for_each(vec.begin(), vec.end(), &boost::checked_delete<string>);
}
