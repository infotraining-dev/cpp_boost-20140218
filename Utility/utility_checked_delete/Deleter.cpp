#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <boost/checked_delete.hpp>
#include <boost/scoped_ptr.hpp>

void Deleter::delete_it(ToBeDeleted* p)
{
    //delete p;
    boost::checked_delete(p);
}
