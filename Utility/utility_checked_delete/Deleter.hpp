#ifndef DELETER_HPP_
#define DELETER_HPP_

#include <boost/shared_ptr.hpp>

class ToBeDeleted;

class Deleter
{
public:
	void delete_it(ToBeDeleted* p);
    void foo(boost::shared_ptr<ToBeDeleted> arg);
};

#endif /* DELETER_HPP_ */
