#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_pointer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/make_shared.hpp>


class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
    BOOST_STATIC_ASSERT(sizeof(int) == 4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}

// custom trait

template <typename T>
struct IsSmartPtr : public boost::false_type
{
    const static bool value = false;
};

template <typename T>
struct IsSmartPtr<boost::shared_ptr<T> > : public boost::true_type
{};

template <typename T>
struct IsSmartPtr<boost::intrusive_ptr<T> > : public boost::true_type
{
};

template <typename T>
void derefence(T ptr)
{
    BOOST_STATIC_ASSERT((IsSmartPtr<T>::value || boost::is_pointer<T>::value) && "Nie jest wskaznikiem");

    std::cout << *ptr << std::endl;
}

class Bad
{
public:
    int operator*() { return 13; }
};


int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    works_with_base_and_derived(arg);

    // custom trait

    boost::shared_ptr<std::string> ptr_str = boost::make_shared<std::string>("Text");
    std::string text = "Inny tekst";

    Bad b;

    std::cout << *b << std::endl;

    derefence(ptr_str);
    derefence(&text);
    derefence(b);
}
