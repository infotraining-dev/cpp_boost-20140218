#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_float.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_void.hpp>

using namespace std;


void foo(int arg)
{
    cout << "foo(int): " << arg << endl;
}

struct Int
{
    int value;

    typedef int type;
};

ostream& operator<<(ostream& out, const Int& i)
{
    out << i.value;

    return out;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T> >::type foo(const T& arg)
{
    cout << "foo(T): " << arg << endl;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T>, ostream&>::type serialize(ostream& out, const T& value)
{
    out << value << endl;

    return out;
}

template <typename T>
typename boost::enable_if<boost::is_integral<T>, ostream&>::type serialize(ostream& out, const T& value)
{
    out << sizeof(value) << " " << value << endl;

    return out;
}

template <typename T, typename Enable = void>
class DataProcessor
{
    BOOST_STATIC_ASSERT(boost::is_void<Enable>::value);

public:
    void process_data(const T* arr, size_t size)
    {
        cout << "DataProcessor<T>::process_data()" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_float<T> >::type>
{
public:
    void process_data(const T* arr, size_t size)
    {
        cout << "DataProcessor<float || double>::process_data()" << endl;
    }
};

int main()
{
    int x = 10;
    double dx = 3.14;
    short sx = 120;
    unsigned int ux = 10;

    foo(x);  // foo(int)
    foo(dx);
    foo(sx);
    foo(ux);

    Int y { 13 };

    foo(y);

    string str = "Text";

    serialize(cout, str);

    serialize(cout, sx);

    int tab1[100];

    DataProcessor<int> dp_int;
    dp_int.process_data(tab1, 100);

    double tab2[100];

    DataProcessor<double> dp_dbl;
    dp_dbl.process_data(tab2, 100);
}

