#include <iostream>
#include <boost/utility.hpp>

class CodeBreaker
{
public:
	int operator&() const
	{
		return 13;
	}
};

template <typename T>
void print_address(const T& t)
{
	std::cout << "Adres: " << (&t) << std::endl;
}

template <typename T>
void smart_print_address(const T& t)
{
	std::cout << "Adres: " << boost::addressof(t) << std::endl;
}


template <typename T>
void how_it_works(const T& t)
{
    T& ref_t = const_cast<T&>(t);
    char& ref_ch = reinterpret_cast<char&>(ref_t);
    char* ptr_ch = &ref_ch;
    T* ptr_t = reinterpret_cast<T*>(ptr_ch);

    std::cout << "Adres: " << boost::addressof(t) << std::endl;
}

int main()
{
	CodeBreaker cb;

	print_address(cb);
	smart_print_address(cb);
    how_it_works(cb);
}
