#include <iostream>
#include <iterator>
#include <list>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility/enable_if.hpp>
#include <cstring>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last) do kontenera rozpoczynającego się od dest
template <typename InIt, typename OutIt>
void mcopy(InIt first, InIt last, OutIt dest)
{
    cout << "Generic mcopy!" << endl;

    while(first != last)
    {
        *(dest++) = *(first++);
    }
}

// 2 - napisz zoptymalizowaną wersję mcopy wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
template <typename T>
void mcopy(T* first, T* last, T* dest, typename boost::enable_if<boost::is_pod<T> >::type* = 0)
{
    cout << "Optimized mcopy!" << endl;

    memcpy(dest, first, distance(first, last) * sizeof(T));
}

int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";
}

