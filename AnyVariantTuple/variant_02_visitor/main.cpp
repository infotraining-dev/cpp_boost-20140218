#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <numeric>
#include <boost/variant.hpp>
#include <boost/bind.hpp>

using namespace std;

void times_two(boost::variant<int, string, complex<double> >& v)
{
	if (int* pi = boost::get<int>(&v))
		*pi *= 2;
	else if (string* pstr = boost::get<string>(&v))
		*pstr += *pstr;
}

class TimesTwoVisitor : public boost::static_visitor<>
{
public:
	void operator()(int& i) const
	{
		i *= 2;
	}

	void operator()(std::string& str) const
	{
		str += str;
	}

    void operator()(std::complex<double>& c) const
    {
        c += c;
    }
};

class Sum : public boost::static_visitor<>
{
public:
    int value;

    Sum() : value(0) {}

    template <typename T>
    void operator()(const T&)
    {}

    void operator()(int x)
    {
        value += x;
    }
};

class Accumulator : private boost::static_visitor<int>
{
public:
    typedef int result_type;

    template <typename T>
    int operator()(const T&)
    { return 0; }

    int operator()(int x)
    {
       return x;
    }

    template <typename Variant>
    int operator()(int before, const Variant& var)
    {
        return before + boost::apply_visitor(*this, var);
    }
};

int main()
{
    boost::variant<int, string, complex<double> > var;

	var = 5;
    cout << "var.which() = " << var.which() << " type = " << var.type().name() << endl;

	times_two(var);
	cout << "var = " << var << endl;

	var = "Tekst...";
	times_two(var);
	cout << "var = " << var << endl;

    // to samo z wizytorem
	boost::apply_visitor(TimesTwoVisitor(), var);
	cout << "var = " << var << endl;

    // wizytacja opozniona
	vector<boost::variant<int, string> > vars;
	vars.push_back(1);
	vars.push_back("two");
	vars.push_back(3);
	vars.push_back("four");

	TimesTwoVisitor visitor;
	for_each(vars.begin(), vars.end(), boost::apply_visitor(visitor));

	cout << "vars: ";
	copy(vars.begin(), vars.end(), ostream_iterator<boost::variant<int, string> >(cout, " "));
	cout << endl;

    Sum sum;
    for_each(vars.begin(), vars.end(), boost::apply_visitor(sum));
    cout << "Sum: " << sum.value << endl;


    cout << "Sum: " << accumulate(vars.begin(), vars.end(), 0, Accumulator()) << endl;
}
