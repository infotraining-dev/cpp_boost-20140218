#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>

using namespace std;

//-------------------------------------------------------------------------------------------------
// wypisanie krotki - metaprogram
template <typename Tuple, int Index>
struct PrintHelper
{
	static void print(const Tuple& t)
	{
        PrintHelper<Tuple, Index-1>::print(t);
        cout << boost::tuples::get<Index>(t) << endl;
	}
};

template <typename Tuple>
struct PrintHelper<Tuple, 0>
{
	static void print(const Tuple& t)
	{
		cout << boost::tuples::get<0>(t) << endl;
	}
};

template <typename Tuple>
void print_all(const Tuple& t)
{
	PrintHelper<Tuple, boost::tuples::length<Tuple>::value-1>::print(t);
}


// ------------------------------------------------------------------------------------------------
// odpowiednik for_each dla krotki
template <typename Tuple, typename Function>
void for_each_element(Tuple& t, Function func)
{
	func(t.get_head());
	for_each_element(t.get_tail(), func);
}

template <typename Function>
void for_each_element(const boost::tuples::null_type&, Function)
{
}

class Printer
{
public:
    template <typename T>
    void operator()(const T& t) const
    {
        cout << t << endl;
    }
};

int main()
{
	boost::tuple<int, string, double> t1(13, "trzynascie", 13.1313);

	cout << "print_all_1(t1):\n";
	print_all(t1);

	cout << "\n\nfor_each_element(t1, Printer()):\n";
	for_each_element(t1, Printer());
}

