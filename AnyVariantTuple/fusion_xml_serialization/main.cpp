#include <iostream>
#include <typeinfo>
#include <string>
#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/view.hpp>
#include <boost/fusion/include/make_vector.hpp>
#include <boost/fusion/include/vector_tie.hpp>
#include <boost/fusion/include/map.hpp>
#include <boost/fusion/include/make_map.hpp>
#include <boost/fusion/include/map_tie.hpp>
#include <boost/fusion/include/has_key.hpp>
#include <boost/fusion/sequence.hpp>
#include <boost/utility/enable_if.hpp>

using namespace boost;

namespace Version1
{
    struct Employee
    {
        int age;
        std::string surname;
        std::string forname;
        double salary;
    };

    std::ostream& format_employee(std::ostream& out, const Employee& e)
    {
        return out << "<int>" << e.age << "</int>"
            << "<string>" << e.surname << "</string>"
            << "<string>" << e.forname << "</string>"
            << "<double>" << e.salary << "</double>";
    }
}

namespace Version2
{
    typedef fusion::vector<int, std::string, std::string, double> Employee;

    struct FormatMember
    {
        explicit FormatMember(std::ostream& out) : out_(out)
        {}

        template <typename T>
        void operator()(const T& value) const
        {
            std::string type = typeid(T).name();
            out_ << '<' << type << '>' << value << "</" << type << ">";
        }

    private:
        std::ostream& out_;
    };

    std::ostream& format_employee(std::ostream& out, const Employee& e)
    {
        fusion::for_each(e, FormatMember(out));

        return out;
    }

    std::ostream& format_employee(std::ostream& out, const Version1::Employee& e)
    {
        fusion::for_each(fusion::vector_tie(e.age, e.surname, e.forname, e.salary), FormatMember(out));

        return out;
    }
}


namespace Version3
{
    namespace keys
    {
        struct surname;
        struct forname;
        struct age;
        struct salary;
        struct employee_number;
    }

    template <typename Key, typename Map>
    void format_optional(std::ostream& out, const Map& m, const std::string& prefix, const std::string& postfix,
                         typename enable_if<fusion::result_of::has_key<Map, Key> >::type* ptr = 0)
    {
        out << prefix << fusion::at_key<Key>(m) << postfix;
    }

    template <typename Key, typename Map>
    void format_optional(std::ostream& out, const Map& m, const std::string& prefix, const std::string& postfix,
                         typename disable_if<fusion::result_of::has_key<Map, Key> >::type* ptr = 0)
    {
    }

    template <typename Map>
    std::ostream& format(std::ostream& out, const Map&  m)
    {
        format_optional<keys::surname>(out, m, "<b>Surname: ", "</b>\n");
        format_optional<keys::forname>(out, m, "Forname: ", "\n");
        format_optional<keys::age>(out, m, "Age: ", " years\n");
        format_optional<keys::salary>(out, m, "Salary: $","\n");
        format_optional<keys::employee_number>(out, m, "Employee no: ", "\n");

        return out;
    }

    std::ostream& format_employee(std::ostream& out, const Version1::Employee& e)
    {
        format(out,
               fusion::map_tie<keys::age, keys::surname, keys::forname, keys::salary>(e.age, e.surname, e.forname, e.salary));

        return out;
    }

    struct FormatMember
    {
        explicit FormatMember(std::ostream& out) : out_(out)
        {}

        template <typename T>
        void operator()(const T& t) const
        {
            std::string fieldname = fusion::at_c<1>(t);
            out_ << '<' << fieldname << '>' << fusion::at_c<0>(t) << "</" << fieldname << ">";
        }

    private:
        std::ostream& out_;
    };

    std::ostream& format_xml(std::ostream& out, const Version1::Employee& e)
    {
        fusion::for_each(
            fusion::zip(fusion::vector_tie(e.age, e.surname, e.forname, e.salary), fusion::make_vector("age", "surname", "forname", "salary")), FormatMember(out));

        return out;
    }
}

int main()
{
    using namespace std;


    Version1::Employee emp1{ 1, "Jan", "Kowalski", 6500.0 };

    format_employee(cout, emp1);

    cout << "\n\n";

    Version2::Employee emp2{ 1, "Adam", "Nowak", 8000.0 };

    Version2::format_employee(cout, emp2);

    cout << "\n\n";

    Version2::format_employee(cout, emp1);

    cout << "\n\n";

    Version3::format_employee(cout, emp1);

    cout << "\n";

    Version3::format_xml(cout, emp1);

    cout << "\n\n";
}
