#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/fusion/include/vector_tie.hpp>
#include <boost/fusion/sequence.hpp>

using namespace std;

struct X
{
    int a, b;
    string c;
};

bool operator==(const X& a, const X& b)
{
    return boost::fusion::vector_tie(a.a, a.b, a.c) == boost::fusion::vector_tie(b.a, b.b, b.c);
}

boost::tuple<int, string> foo()
{
	return boost::make_tuple(10, "Zenon Kowalski");
}

boost::tuple<int, int> divide(int x, int y)
{
    return boost::make_tuple(x / y, x % y);
}

int main()
{
	int id;
	string name;

	boost::tie(id, name) = foo();

	cout << "id = " << id << "; name = " << name << endl;

	// tie dziala rowniez z pair
	boost::tie(id, name) = make_pair(1, "Nikodem Dyzma");

	cout << "id = " << id << "; name = " << name << endl;

	// tuple::ignore

	string person;

	boost::tie(boost::tuples::ignore, person) = foo();

	cout << "name = " << person << endl;

    int reszta;
    int wynik;

    boost::tie(wynik, reszta) = divide(5, 2);
}



