#include <iostream>
#include <boost/any.hpp>

using namespace std;

int main()
{
    using namespace boost;

    any a1(8);

    cout << "a1 is empty = " << a1.empty() << endl;

    a1 = 10;
    a1 = 3.14;
    a1 = string("Tekst");

    cout << "a1 type = " << a1.type().name() << endl;

    try
    {
        string tekst = any_cast<string>(a1);

        cout << "tekst = " << tekst << endl;

        string* ptr_text = any_cast<string>(&a1);

        cout << "tekst = " << *ptr_text << endl;
    }
    catch(const bad_any_cast& e)
    {
        cout << e.what() << endl;
    }
}

