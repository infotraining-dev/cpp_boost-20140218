#include <iostream>
#include <vector>
#include <algorithm>

#include <boost/variant.hpp>

struct Model_t; // leaf node
struct Sound_t; // leaf node
struct Group_t; // internal node

typedef boost::variant<
	Model_t,
	Sound_t,
	boost::recursive_wrapper<Group_t>
> Node_t;

struct Model_t
{
	void Draw() const { std::cout << "Model_t::Draw()\n"; }
};

struct Sound_t
{
	void Play() const { std::cout << "Sound_t::Play()\n"; }
};

struct Group_t
{
	void Attach(const Node_t &Kid) { m_Kids.push_back(Kid); }

	template <typename VisT>
	void apply_visitor(const VisT &Vis) const
	{
		std::for_each(
			m_Kids.begin(), m_Kids.end(),
			boost::apply_visitor(Vis)
			);
	}

private:

	std::vector<Node_t> m_Kids;
};

template <typename VisT>
struct SceneVisitor_T : public boost::static_visitor<void>
{
	template <typename ElemT> // generic empty handler
	void operator()(const ElemT &) const {}

	void operator()(const Group_t &Group) const
	{
		Group.apply_visitor(*static_cast<const VisT *>(this));
	}
};

struct RenderVisitor_t : public SceneVisitor_T<RenderVisitor_t> // CRTP
{
	typedef SceneVisitor_T<RenderVisitor_t> Base_t;
	using Base_t::operator(); // prevent operator() hiding

	void operator()(const Model_t &Model) const
	{
		Model.Draw();
	}
};

struct AudioVisitor_t : public SceneVisitor_T<AudioVisitor_t> // CRTP
{
	typedef SceneVisitor_T<AudioVisitor_t> Base_t;
	using Base_t::operator(); // prevent operator() hiding

	void operator()(const Sound_t &Sound) const
	{
		Sound.Play();
	}
};

int main()
{
	Group_t Group;
	Group.Attach(Model_t());
	Group.Attach(Sound_t());

	Group_t Root;
	Root.Attach(Model_t());
	Root.Attach(Sound_t());
	Root.Attach(Group);

	boost::apply_visitor(RenderVisitor_t(), Root);
	boost::apply_visitor(AudioVisitor_t(), Root);

	system("PAUSE");
}

// console output:
// Model_t::Draw()
// Model_t::Draw()
// Sound_t::Play()
// Sound_t::Play()
